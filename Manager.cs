public class Manager
{
	public int DoSomething(List<OrderData> results, int storeId)
	{
		decimal priceTotal = 0;
		decimal sumTotal = 0;
		
		OrderRepository orderRepository = new OrderRepository();
		StoreRepository storeRepository = new StoreRepository();
		Store store = storeRepository.GetStore(storeId);
		foreach (OrderData result in results)
		{
			priceTotal = result.ItemPrice + result.ItemPrice;
		}
		switch (store.StateCode)
		{
			case "CA":
				if (priceTotal >= 100)
					sumTotal = (priceTotal * store.TaxRate) + priceTotal;
				break;
			case "TX":
				if (priceTotal >= 100)
					sumTotal = (priceTotal * store.TaxRate) + priceTotal;
				break;
			case "AZ":
				if (priceTotal >= 110)
					sumTotal = (priceTotal * store.TaxRate) + priceTotal;
				break;
		}
		foreach (OrderData result in results)
		{
			orderRepository.Save(result);
		}
		return sumTotal;
	}
}
